require 'gnuplot'

# Params to be modified by user
#
# Iterations
TOP = 499
BOT = 9
STEP = 10
# Plot parameters
param = {:xlabel => "node (ds = 10% debye lenght",
         :ylabel => "field (simulation units)"}
#Format name: format of the file name in which you change KEY by the value the iterator will have
FNAME = "data/avg_charge_t_KEY.dat"
# Vibration flag
FIXSCALE = true
# Ratio of the y axis that needs to be occupied
# 1/3 will leave 1/3 of the plot with data 1/3 on top and bottom white
YRATIO = {:up => 1.0/15, :down => 1.0/15}
XRATIO = {:up => 0, :down => 0}
#Manual scale
# Example: EXTREMES = {:xmin => -1, :xmax => 1, :ymin => -1, :ymax => 1}
EXTREMES = {} #Leave empty to avoid it
#------------------------------------------------------

puts "*****************************"
puts " Movie script for Tejero xD "
puts "*****************************"

#Find max and min 
if FIXSCALE and EXTREMES.length == 0
    x = []
    y = []
    if XRATIO[:up]+XRATIO[:down] >= 1
        puts "XRATIO cannot have components adding more than 1"
        exit
    end
    if YRATIO[:up]+YRATIO[:down] >= 1
        puts "YRATIO cannot have components adding more than 1"
        exit
    end
    puts "  -- Finding max and min ranges..."
    (BOT..TOP).step(STEP) do |iter|
        File.open("#{FNAME.gsub("KEY",iter.to_s)}","r").each_line do |line|
            x << line.split[0].to_f
            y << line.split[1].to_f
        end
    end
    xmax = x.max
    xmin = x.min
    ymax = y.max
    ymin = y.min

    #Derive extremes
    xl = xmax - xmin
    xd = xl/(1-XRATIO[:down]-XRATIO[:up])
    yl = ymax - ymin
    yd = yl/(1-YRATIO[:down]-YRATIO[:up])
    $extremes = {:xmin => xmin-XRATIO[:down]*xd, :xmax => xmax+XRATIO[:up]*xd, :ymin => ymin-YRATIO[:down]*yd, :ymax => ymax+YRATIO[:up]*yd}
    puts "     Done!"
end
$extremes = EXTREMES if EXTREMES.length != 0 
    
puts "  -- Doing frames..."
$counter = -1
Gnuplot.open do |gp|
    saved = -1
    actual = -1
    (BOT..TOP).step(STEP) do |iter|
        Gnuplot::Plot.new(gp) do |plot|
            plot.terminal "jpeg size 1280,720" 
            plot.nokey
            plot.grid
            plot.ylabel param[:ylabel]
            plot.xlabel param[:xlabel]
            if FIXSCALE or EXTREMES.length != 0 
                plot.xrange "[#{$extremes[:xmin]}:#{$extremes[:xmax]}]"
                plot.yrange "[#{$extremes[:ymin]}:#{$extremes[:ymax]}]"
            end
            plot.title "field (t = #{iter})"
            plot.output File.expand_path("./field_movie_#{$counter += 1}.jpg")
            #puts "Reading filei data/#{FNAME.gsub("KEY",iter.to_s)}"
            #plot.data << Gnuplot::DataSet.new(FNAME.gsub("KEY",iter.to_s))
            plot.arbitrary_lines << "plot \"#{FNAME.gsub("KEY",iter.to_s)}\""
        end
    end
end
puts "     Done!"
